import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Test from './views/Test.vue'
import Valeri from './views/Valeri.vue'
import ZnaiMesta from './views/ZnaiMesta.vue'
import ProductPage from './views/ProductPage.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/productpage',
      name: 'productpage',
      component: ProductPage
    },
    {
      path: '/valeri',
      name: 'valeri',
      component: Valeri
    },
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/znaimesta',
      name: 'znaimesta',
      component: ZnaiMesta
    },
    {
      path: '/test',
      name: 'test',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: Test
    },
  ]
})
