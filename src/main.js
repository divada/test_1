import Vue from 'vue'
import JQuery from 'jquery'
import App from './App.vue'
import store from './store'
import Vuetify from 'vuetify'
import router from './router'
import VueHead from 'vue-head'
import vSelect from 'vue-select'
import '@/styles/scss/_init.scss'
import '@/styles/scss/_templates.scss'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import VueClipboard from 'vue-clipboard2'
import * as VueThreejs from 'vue-threejs'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import fas from '@fortawesome/fontawesome-free-solid'
import fab from '@fortawesome/fontawesome-free-brands'
import far from '@fortawesome/fontawesome-free-regular'
import VueProgressiveImage from 'vue-progressive-image'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faCoffee } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(fas)
library.add(fab)
library.add(far)

let $ = JQuery

Vue.component('fa', FontAwesomeIcon)

Vue.use(VueHead)
Vue.use(Vuetify)
Vue.use(VueThreejs)
Vue.use(BootstrapVue)
Vue.use(VueProgressiveImage)

Vue.prototype.$eventBus = new Vue()

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
