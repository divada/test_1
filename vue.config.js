module.exports = {
  lintOnSave: undefined,
  devServer: {
    public: 'divada.sweetrewenge.com:1337'
  },
  outputDir: '../public',
  indexPath: process.env.NODE_ENV === 'production'
    ? '../resources/views/index.blade.php'
    : 'index.html',
  css: {
    loaderOptions: {
      sass: {
        data: `@import "@/styles/scss/_init.scss";`
      }
    }
  },
}
